#define GLFW_DLL

// OpenGLの新しい機能を使うために
// ライブラリ「GLEW」を利用
// そのライブラリをスタティックリンク形式で使う

#define GLEW_STATIC

#if defined (_MSC_VER)
// GLEWのヘッダファイルを使う(Windows)
#include<GL/glew.h>
#endif
#include<GL/glfw.h>
#include<fstream>
#include<vector>

#if defined (_MSC_VER)
#pragma comment(lib,"GLFWDLL.lib")
#pragma comment(lib,"opengl32.lib")

#ifdef _DEBUG
#pragma comment(lib,"glew32sd.lib")
#else
#pragma comment(lib,"glew32s.lib")
#endif

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform2.hpp>

#endif




int main(){
	if (!glfwInit()){
		return EXIT_FAILURE;
	}

	if (!glfwOpenWindow(0, 0, 0, 0, 0, 0, 0, 0, GLFW_WINDOW)){
		glfwTerminate();
		return EXIT_FAILURE;
	}
	glfwSwapInterval(1);

#if defined(_MSC_VER)
	//GLEWを初期化
	if (glewInit() != GLEW_OK){
		glfwTerminate();
		return EXIT_FAILURE;
	}
#endif


	while (glfwGetWindowParam(GLFW_OPENED)){

		const GLubyte *renderer = glGetString(GL_RENDERER);
		const GLubyte *vendor = glGetString(GL_VENDOR);

		const GLubyte *version = glGetString(GL_VERSION);
		const GLubyte *glslversion = glGetString(GL_SHADING_LANGUAGE_VERSION);

		GLint major, minor;

		glGetIntegerv(GL_MAJOR_VERSION, &major);
		glGetIntegerv(GL_MINOR_VERSION, &minor);

		printf("GL Vendor	:%s\n",vendor);
		printf("GL Render	:%s\n", renderer);
		printf("GL Version(string)	:%s\n", version);
		printf("GL Version(integer)	:%d,%d,\n", major,minor);
		printf("GLSL Version		:%s\n", glslversion);

			glfwSwapBuffers();
	}
	glfwTerminate();

	return EXIT_SUCCESS;
}
