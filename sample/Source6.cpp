#define GLFW_DLL

#include <GL/glfw.h>
#include <cstdlib>

#if defined(_MSC_VER)
#pragma comment (lib,"GLFWDLL.lib")
#pragma comment (lib,"opengl32.lib")
#endif

int main(){
	if (!glfwInit()){
		return EXIT_FAILURE;
	}

	if (!glfwOpenWindow(0, 0, 0, 0, 0, 0, 0, 0, GLFW_WINDOW)){
		glfwTerminate();
		return EXIT_FAILURE;
	}

	glfwSwapInterval(1);

	while (glfwGetWindowParam(GLFW_OPENED)){
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		//描画する三角形の3頂点を用意
		static const GLfloat vtx[] = {
			0.0f, 0.433f,
			-0.5f, -0.433f,
			0.5f, -0.433f
		};
		glVertexPointer(2, GL_FLOAT, 0, vtx);

		//頂点一つ一つに対する色を配列で用意
		//赤、緑、青の成分をそれぞれ
		//0.0~1.0で指定する
		static const GLfloat color[] = {
			1.0f, 0.0f, 0.0f,1.0f,
			0.0f, 1.0f, 0.0f,0.5f,
			0.0f, 0.0f, 1.0f,1.0f
		};
		//色配列をOpenGLに指示
		glColorPointer(4, GL_FLOAT, 0, color);

		//頂点配列を使った描画を有効にする
		glEnableClientState(GL_VERTEX_ARRAY);

		//色配列を使った描画を有効にする
		glEnableClientState(GL_COLOR_ARRAY);

		glDrawArrays(GL_TRIANGLES, 0, 3);

		//描画が終わったら有効にした機能を無効にしておく
		glDisableClientState(GL_VERTEX_ARRAY);
		glDisableClientState(GL_COLOR_ARRAY);

		glfwSwapBuffers();
	}
	glfwTerminate();

	return EXIT_SUCCESS;
}