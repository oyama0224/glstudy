#define GLFW_DLL

#include "GL/glfw.h"
#include <cstdlib>

#if defined(_MSC_VER)
#pragma comment(lib, "GLFWDLL.lib")
#pragma comment(lib, "opengl32.lib")

#endif

int main(){
	if (!glfwInit()){
		return EXIT_FAILURE;
	}

	if (!glfwOpenWindow(640, 480, 0, 0, 0, 0, 0, 0, GLFW_WINDOW))
	{
		glfwTerminate();
		return EXIT_FAILURE;
	}

	glfwSwapInterval(1);

	while (glfwGetWindowParam(GLFW_OPENED)){
		//描画バッファを塗りつぶす色の成分を
		//それぞれ0.0~1.0で指定
		//glClearColor(赤,緑,青,アルファ)
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

		//描画バッファを塗りつぶす
		glClear(GL_COLOR_BUFFER_BIT);

		//描画する[点]のX座標とY座標を配列で用意
		static const GLfloat vtx[] = { 0.0f, 0.0f };

		//描画する頂点の配列をOpenGLに指示する
		glVertexPointer(2, GL_FLOAT, 0, vtx);

		//描画する[点]の大きさを指定
		glPointSize(10.0f);

		//描画する[点]の色の成分を
		//それぞれ0.0〜1.0で指定
		glColor4f(1.0f, 1.0f, 1.0f, 1.0f);

		//頂点配列で描画するモードに切り替えて
		//[点]の描画を開始
		glEnableClientState(GL_VERTEX_ARRAY);
		glDrawArrays(GL_POINTS, 0, 1);

		//描画が終わったら描画モードを元に戻す
		glDisableClientState(GL_VERTEX_ARRAY);

		glfwSwapBuffers();
	}
	glfwTerminate();


	return EXIT_SUCCESS;
}