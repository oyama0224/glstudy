#define GLFW_DLL

#include <gl/glfw.h>
#include <cstdlib>

#if defined (_MSC_VER)

#pragma comment(lib,"GLFWDLL.lib")
#pragma comment(lib,"opengl32.lib")
#endif

int main(){
	if (!glfwInit()){
		return EXIT_FAILURE;
	}

	//サイズ640*480のウィンドウを作る
	if (!glfwOpenWindow(640, 480, 0, 0, 0, 0, 0, 0, GLFW_WINDOW)){
		glfwTerminate();
		return EXIT_FAILURE;
	}

	glfwSwapInterval(1);

	//描画する点の初期座標を定義
	float pos_x = 0.0f;
	float pos_y = 0.0f;

	while (glfwGetWindowParam(GLFW_OPENED)){
		// ESCが押されたらメインループを抜ける
		if (glfwGetKey(GLFW_KEY_ESC) == GLFW_PRESS)
			break;

		static const float speed = 0.02f;
		//A,D,W,Xそれぞれのキーが押されたら、点の座標を変更する
		if (glfwGetKey('A') == GLFW_PRESS){
			pos_x -= speed;
		}
		if (glfwGetKey('D') == GLFW_PRESS){
			pos_x += speed;
		}
		if (glfwGetKey('W') == GLFW_PRESS){
			pos_y += speed;
		}
		if (glfwGetKey('X') == GLFW_PRESS){
			pos_y -= speed;
		}
		//マウスの左ボタンがおｓれていたら
		//マウスのカーソル位置への点の座標を変更
		if (glfwGetMouseButton(GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS){
			int mouse_X, mouse_Y;

			glfwGetMousePos(&mouse_X, &mouse_Y);
			//マウスの座標(0.0(639,479)を
			//スクリーン座標(-1,-1)〜(0.1,0.1)に変換

			pos_x = mouse_X * 2.0f / 640.0f - 1.0f;
			pos_y = -(mouse_Y * 2.0f / 480.0f - 1.0f);
		}

		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		//描画する点の頂点配列を作成
		//このようにして、変数を使った作成もできる

		const GLfloat vtx[] = {
			pos_x, pos_y
		};

		glVertexPointer(2, GL_FLOAT, 0, vtx);

		glEnableClientState(GL_VERTEX_ARRAY);

		glDrawArrays(GL_POINTS, 0, 1);

		glDisableClientState(GL_VERTEX_ARRAY);

		glfwSwapBuffers();
	}

	glfwTerminate();

	return EXIT_SUCCESS;
}