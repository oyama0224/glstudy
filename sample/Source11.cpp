﻿#define GLFW_DLL

#include<GL/glfw.h>
#include<cstdlib>

#if defined (_MSC_VER)
#pragma comment(lib,"GLFWDLL.lib")
#pragma comment(lib,"opengl32.lib")
#endif

int main(){
	if (!glfwInit()){
		return EXIT_FAILURE;
	}

	//500*400ピクセルのウィンドウを用意

	if (!glfwOpenWindow(640, 480, 0, 0, 0, 0, 0, 0, GLFW_WINDOW)){
		glfwTerminate();
		return EXIT_FAILURE;
	}
	glfwSwapInterval(1);

	//「ビューポート変換」を指定
	//glViewport(X座標, Y座標,幅,高さ)
	//画面⇐したからの座標(X,Y)から
	//(幅*高さ)ピクセルの領域を描画領域とする
	glViewport(0, 0, 640, 480);

	//「投影行列」を操作対象にする
	glMatrixMode(GL_PROJECTION);
	// 単位行列を読み込むように指示
	glLoadIdentity();

	//正投影行列を作り、現在の行列に掛け合わせる
	//glOrtho(左X座標, 右X座標,下Y座標,上Y座標,手前Z座標,奥Z座標)
	glOrtho(0.0f, 640.0f, 480.0f, 0.0f, -1.0f, 1.0f);

	while (glfwGetWindowParam(GLFW_OPENED)){
		glClearColor(0.5f, 0.5f, 0.5f, 0.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		//(100,100) - (200,200)の矩形を表示するための4頂点を用意
		static const GLfloat vtx[] = {
			100.0f, 100.0f,
			300.0f, 100.0f,
			300.0f, 300.0f,
			100.0f, 300.0f
		};
		glVertexPointer(2, GL_FLOAT, 0, vtx);
		glColor4f(1.0f, 1.0f, 1.0f, 1.0f);

		glEnableClientState(GL_VERTEX_ARRAY);

		glDrawArrays(GL_QUADS, 0, 4);

		glDisableClientState(GL_VERTEX_ARRAY);

		glfwSwapBuffers();
	}
	glfwTerminate();
	return EXIT_SUCCESS;
}