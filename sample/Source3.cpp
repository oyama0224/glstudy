#define GLFW_DLL

#include <GL/glfw.h>
#include <cstdlib>

#if defined (_MSC_VER)
#pragma comment(lib,"GLFWDLL.lib")
#pragma comment(lib,"opengl32.lib")
#endif

int main(){
	if (!glfwInit()){
		return EXIT_FAILURE;
	}

	if (!glfwOpenWindow(640, 480, 0, 0, 0, 0, 0, 0, GLFW_WINDOW)){
		glfwTerminate();
		return EXIT_FAILURE;
	}
	glfwSwapInterval(1);
	while (glfwGetWindowParam(GLFW_OPENED)){
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

		glClear(GL_COLOR_BUFFER_BIT);

		static const GLfloat vtx[] =
		{
			-0.9f, -0.9f,
			0.5f, 0.5f
		};


		glVertexPointer(2, GL_FLOAT, 0, vtx);

		//線分の太さを指示
		glLineWidth(10.0f);

		glColor4f(1.0f, 0.0f, 1.0f, 1.0f);

		glEnableClientState(GL_VERTEX_ARRAY);

		//OpenGLに線分の描画を指示
		glDrawArrays(GL_LINES, 0, 2);

		glDisableClientState(GL_VERTEX_ARRAY);

		glfwSwapBuffers();
	}
	glfwTerminate();

	return EXIT_SUCCESS;
}

