#define GLFW_DLL

#include <GL/glfw.h>
#include <cstdlib>

#if defined (_MSC_VER)

#pragma comment(lib, "GLFWDLL.lib")
#pragma comment(lib, "opengl32.lib")

#endif


int main(){
	if (!glfwInit()){
		return EXIT_FAILURE;
	}

	//描画ウィンドウの「幅」「高さ」を
	//最初の二つの引数で指定（単位:ピクセル）

	if (!glfwOpenWindow(640, 800, 0, 0, 0, 0, 0, 0, GLFW_WINDOW))
	{
		glfwTerminate();
		return EXIT_FAILURE;
	}
	glfwSwapInterval(1);

	while (glfwGetWindowParam(GLFW_OPENED)){
		glfwSwapBuffers();
	}
	glfwTerminate();

	return EXIT_SUCCESS;

}