#define GLFW_DLL

//OpenGL拡張を使うためにGLEWを
//スタティックリンク形式で使う

#define GLEW_STATIC

#if defined(_MSC_VER)
//GLEWのヘッダファイルを利用
#include<GL/glew.h>
#endif
#include<GL/glfw.h>
#if defined(_APPLE_)
// OpenGL拡張を使うために
// glext.hをインクルード
#include<openGL/gltext.h>
#endif
#include<cstdlib>
//関数sin()を使うためのヘッダをインクルード
#include<cmath>

#if defined (_MSC_VER)
#pragma comment(lib,"GLFWDLL.lib")
#pragma comment(lib,"opengl32.lib")
//ライブラリ「GLEW」のリンク指定
#ifdef _DEBUG
#pragma comment(lib,"glew32sd.lib")
#else
#pragma comment(lib,"glew32s.lib")
#endif
#endif

int main(){
	if (!glfwInit()){
		return EXIT_FAILURE;
	}

	if (!glfwOpenWindow(640, 480, 0, 0, 0, 0, 0, 0, GLFW_WINDOW)){
		glfwTerminate();
		return EXIT_FAILURE;
	}
	glfwSwapInterval(1);

#if defined (_MSC_VER)
	//GLEWを初期化(Windows)
	if (glewInit() != GLEW_OK){
		glfwTerminate();
		return EXIT_FAILURE;
	}
#endif
	//「描画バッファ」のフレームバッファ識別子を
	//OpenGLに教えてもらう※後で使う
	GLint current_framebuffer;

	glGetIntegerv(GL_FRAMEBUFFER_BINDING, &current_framebuffer);

	//オフスクリーンバッファ用のテクスチャ識別子を作成
	GLuint fbo_texture_id;
	glGenTextures(1, &fbo_texture_id);

	//「以下の指示は、
	//オフスクリーンバッファ用の
	// テクスチャ識別子に対して使う」
	// と、OpenGLに対して伝える
	glBindTexture(GL_TEXTURE_2D, fbo_texture_id);
	//「オフスクリーンバッファ用のテクスチャは
	// 256*256ピクセル,
	// 1ピクセルあたり[赤、緑、青]の色情報があります」
	// などと、あらかじめ指示を出しておく
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 256, 256, 0, GL_RGB, GL_UNSIGNED_BYTE, 0);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	//オフスクリーンバッファ用の
	//フレームバッファ識別子を
	//１つ作る
	GLuint framebuffer_id;
	glGenFramebuffers(1, &framebuffer_id);

	//「以下の指示は、
	//	オフスクリーンバッファ用のフレームバッファ識別子に対して行う」
	//　とOpenGLに伝える
	glBindFramebuffer(GL_FRAMEBUFFER, framebuffer_id);

	//フレームバッファに
	//先ほど作ったオフスクリーンバッファ用のテクスチャ識別子を結びつける
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, fbo_texture_id, 0);

	float angle = 0.0f;

	while (glfwGetWindowParam(GLFW_OPENED)){
		//「以下の描画命令は、
		// 指定したフレームバッファ識別子に対して行う」
		//とOpenGLに指示する
		glBindFramebuffer(GL_FRAMEBUFFER, framebuffer_id);

		//オフスクリーンバッファ用のテクスチャは
		//256*256ピクセルなので、
		//ビューポート変換をそれにあわせる
		glViewport(0, 0, 256, 256);

		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

		glClear(GL_COLOR_BUFFER_BIT);

		//左右に移動する赤い矩形を描画
		angle += 0.02f;
		const float x_ofs = std::sin(angle)*0.5f;

		const GLfloat vtx[] = {
			-0.5f + x_ofs, -0.5f,
			0.5f + x_ofs, -0.5f,
			0.5f + x_ofs, 0.5f,
			-0.5f + x_ofs,0.5f
		};
		glVertexPointer(2, GL_FLOAT, 0, vtx);
		glColor4f(1.0f, 0.0f, 0.0f, 1.0f);

		glEnableClientState(GL_VERTEX_ARRAY);

		glDrawArrays(GL_QUADS, 0, 4);

		glDisableClientState(GL_VERTEX_ARRAY);
		//以下の描画命令は、
		//「描画バッファ」のフレームバッファ識別子に対して行う
		//とOpenGLに指示する
		glBindFramebuffer(GL_FRAMEBUFFER, current_framebuffer);

		//描画バッファは640*480ピクセルなので
		//ビューポート変換をそれにあわせる
		glViewport(0, 0, 640, 480);
		glClearColor(0.5f, 0.5f, 0.5f, 0.0f);
		glClear(GL_COLOR_BUFFER_BIT);
		//画面にタイル状に矩形を描画する
		for (int y = 0; y < 3; ++y){
			for (int x = 0; x < 3; ++x){
				float x_ofs = x * 0.55f - 0.8f;
				float y_ofs = y * 0.55f - 0.8f;
				const GLfloat vtx2[] = {
					0.0f + x_ofs, 0.0f + y_ofs,
					0.5f + x_ofs, 0.0f + y_ofs,
					0.5f + x_ofs, 0.5f + y_ofs,
					0.0f + x_ofs, 0.5f + y_ofs
				};
				glVertexPointer(2, GL_FLOAT, 0, vtx2);
				glColor4f(1.0f, 1.0f, 1.0f, 1.0f);

				static const GLfloat texture_uv[] = {
					0.0f, 0.0f,
					1.0f, 0.0f,
					1.0f, 1.0f,
					0.0f, 1.0f
				};
				glTexCoordPointer(2, GL_FLOAT, 0, texture_uv);

				glEnable(GL_TEXTURE_2D);
				//描画に
				//オフスクリーンバッファ用の
				//テクスチャ識別子を使う

				glBindTexture(GL_TEXTURE_2D, fbo_texture_id);

				glEnableClientState(GL_VERTEX_ARRAY);

				glEnableClientState(GL_TEXTURE_COORD_ARRAY);
				glDrawArrays(GL_QUADS, 0, 4);
				glDisableClientState(GL_VERTEX_ARRAY);
				glDisableClientState(GL_TEXTURE_COORD_ARRAY);

				glDisable(GL_TEXTURE_2D);
			}
		}
		glfwSwapBuffers();
	}

	//作ったテクスチャ識別子と
	//フレームバッファ識別子を削除
	glDeleteTextures(1, &fbo_texture_id);
	glDeleteFramebuffers(1, &framebuffer_id);

	glfwTerminate();

	return EXIT_SUCCESS;

}
