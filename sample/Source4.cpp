#define GLFW_DLL

#include <GL/glfw.h>
#include <cstdlib>

#if defined (_MSC_VER)
#pragma comment(lib,"GLFWDLL.lib")
#pragma comment(lib,"opengl32.lib")
#endif

int main(){
	if (!glfwInit()){
		return EXIT_FAILURE;
	}

	if (!glfwOpenWindow(0, 0, 0, 0, 0, 0, 0, 0, GLFW_WINDOW)){
		glfwTerminate();
		return EXIT_FAILURE;
	}
	glfwSwapInterval(1);


	while (glfwGetWindowParam(GLFW_OPENED)){
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

		glClear(GL_COLOR_BUFFER_BIT);

		//三角形の3頂点を配列で用意

		static const GLfloat vtx[] = {
			0.0f, 0.5f,
			-0.5f, -0.433f,
			-0.9f, -0.433f
		};

		glVertexPointer(2, GL_FLOAT, 0, vtx);
		glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
		glEnableClientState(GL_VERTEX_ARRAY);

		//OpenGLに三角形の描画を指示
		glDrawArrays(GL_TRIANGLES, 0, 3);

		glDisableClientState(GL_VERTEX_ARRAY);

		glfwSwapBuffers();
	}
	glfwTerminate();

	return EXIT_SUCCESS;
}
