#define GLFW_DLL

// OpenGLの新しい機能を使うために
// ライブラリ「GLEW」を利用
// そのライブラリをスタティックリンク形式で使う

#define GLEW_STATIC

#if defined (_MSC_VER)
// GLEWのヘッダファイルを使う(Windows)
#include<GL/glew.h>
#endif
#include<GL/glfw.h>
#include<fstream>
#include<vector>

#if defined (_MSC_VER)
#pragma comment(lib,"GLFWDLL.lib")
#pragma comment(lib,"opengl32.lib")

#ifdef _DEBUG
#pragma comment(lib,"glew32sd.lib")
#else
#pragma comment(lib,"glew32s.lib")
#endif

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform2.hpp>

#include <iostream>

#endif

GLchar* ShaderAsString(const char* filename)
{
	FILE* file = NULL;
	size_t size = 0;
	GLchar* source;
	int ret;


	errno_t err = fopen_s(&file, filename, "rb");	//ファイルを開く
	if (err != 0){
		glfwTerminate();
		return "-1";
	}
	fseek(file, 0, SEEK_END);//ファイルサイズを算出
	size = ftell(file);

	// ファイルサイズのメモリを確保 //
	source = (GLchar *)malloc(size + 1);
	if (source == "")
	{
		fprintf(stderr, "Could not allocate read buffer.\n");
		return NULL;
	}
	// ファイルを先頭から読み込む //
	fseek(file, 0L, SEEK_SET);
	ret = fread((void *)source, 1, size, file) != (size_t)size;
	fclose(file);
	// シェーダのソースプログラムのシェーダオブジェクトへの読み込み //
	if (ret)
	{
		fprintf(stderr, "Could not read file: %s.\n", filename);
		return NULL;
	}
	// 最後にNULL文字を付与
	source[size] = '\0';
	return source;
}


int main(){

	if (!glfwInit()){
		return EXIT_FAILURE;
	}

	if (!glfwOpenWindow(0, 0, 0, 0, 0, 0, 0, 0, GLFW_WINDOW)){
		glfwTerminate();
		return EXIT_FAILURE;
	}
	glfwSwapInterval(1);

#if defined(_MSC_VER)
	//GLEWを初期化
	if (glewInit() != GLEW_OK){
		glfwTerminate();
		return EXIT_FAILURE;

	}
#endif

	//1.シェーダオブジェクトを次のように作成する
	GLuint VertShader = glCreateShader(GL_VERTEX_SHADER);
	if (0 == VertShader)
	{
		glfwTerminate();
		return EXIT_FAILURE;
	}

	//フラグメントオブジェクトを次のように作成する
	GLuint FragShader = glCreateShader(GL_FRAGMENT_SHADER);
	if (0 == FragShader){
		glfwTerminate();
		return EXIT_FAILURE;
	}


	GLchar* vcode = ShaderAsString("basic.vert");
	const GLchar* vBuf[] = { vcode };
	//2.ソースコードを（場合により複数の場所から）シェーダオブジェクトにコピーします
	glShaderSource(VertShader, 1, vBuf, NULL);
	//3.シェーダをコンパイルします
	glCompileShader(VertShader);


	//4.コンパイルステータスを検証します
	GLint result;
	glGetShaderiv(VertShader, GL_COMPILE_STATUS, &result);
	if (GL_FALSE == result){
		std::cout << "コンパイルに失敗しました";

		GLint logLen;
		glGetShaderiv(VertShader, GL_INFO_LOG_LENGTH, &logLen);
		if (logLen > 0){
			char * log = (char*)malloc(logLen);
			GLsizei written;
			glGetShaderInfoLog(VertShader, logLen, &written, log);

			std::cout << "シェーダログ:", log;
			free(log);
			return EXIT_FAILURE;
		}
	}
	//フラグメントシェーダーのコピー
	GLchar* fcode = ShaderAsString("basic.frag");

	const GLchar* fBuf[] = { fcode };
	glShaderSource(FragShader, 1, fBuf, NULL);
	//フラグメントシェーダーのコンパイル
	glCompileShader(FragShader);
	//フラグメントシェーダのコンパイルチェック
	glGetShaderiv(FragShader, GL_COMPILE_STATUS, &result);
	if (GL_FALSE == result){
		std::cout << "コンパイルに失敗しました";

		GLint logLen;
		glGetShaderiv(FragShader, GL_INFO_LOG_LENGTH, &logLen);
		if (logLen > 0){
			char * log = (char*)malloc(logLen);
			GLsizei written;
			glGetShaderInfoLog(FragShader, logLen, &written, log);

			std::cout << "シェーダログ:", log;
			free(log);
			glfwTerminate();
			return EXIT_FAILURE;
		}
	}
	//プログラムオブジェクトを作成します
	GLuint programHandle = glCreateProgram();
	



	if (0 == programHandle)
	{
		glfwTerminate();
		return EXIT_FAILURE;
	}
	glAttachShader(programHandle, VertShader);
	glAttachShader(programHandle, FragShader);

	//プログラムをリンクします
	glLinkProgram(programHandle);

	//プログラムをリンクします
	glLinkProgram(programHandle);

	//リンク結果を検証します
	GLint status;
	glGetProgramiv(programHandle, GL_LINK_STATUS, &status);
	if (GL_FALSE == status)
	{
		GLint logLen;
		glGetProgramiv(programHandle, GL_INFO_LOG_LENGTH, &logLen);
		if (logLen > 0){
			char* log = (char*)malloc(logLen);
			GLsizei written;
			glGetProgramInfoLog(programHandle, logLen, &written, log);
			free(log);
			glfwTerminate();
			return EXIT_FAILURE;
		}

	}

	while (glfwGetWindowParam(GLFW_OPENED)){
		glClear(GL_COLOR_BUFFER_BIT);
		glUseProgram(programHandle);


		//三角形の3頂点を配列で用意

		static const GLfloat vtx[] = {
			0.0f, 0.5f,
			-0.5f, -0.433f,
			-0.9f, -0.433f
		};
		glVertexPointer(2, GL_FLOAT, 0, vtx);
		glColor3f(1.0, 1.0, 0.0);
		glEnableClientState(GL_VERTEX_ARRAY);

		//OpenGLに三角形の描画を指示
		glDrawArrays(GL_TRIANGLES, 0, 3);
		
		glDisableClientState(GL_VERTEX_ARRAY);


		glfwSwapBuffers();
	}
	glDeleteShader(VertShader);
	glDeleteShader(FragShader);
	glDeleteProgram(programHandle);
	glfwTerminate();

	return EXIT_SUCCESS;
}
