#define GLFW_DLL

#include <gl/glfw.h>
#include <cstdlib>

// C++でファイルを扱うライブラリ
#include<fstream>
#include<vector>

#if defined (_MSC_VER)
#pragma comment(lib,"GLFWDLL.lib")
#pragma comment(lib,"opengl32.lib")
#endif

//指定テクスチャ識別子へ
//ファイルから読み込んだデータを与える
//※処理が成功するとtrueを返す

bool setupTexture(const GLuint id, const char* file){
	std::ifstream fstr(file, std::ios::binary);

	//ファイルが見つからない等のエラーがあれば
	//処理を中断
	if (!fstr) return false;

	//ファイルサイズを取得
	// 読み込み位置をファイル末尾へ移動
	// →ファイル先頭から読み込み位置までのオフセット
	// =ファイルサイズ
	const size_t file_size = static_cast<size_t>(fstr.seekg(0, fstr.end).tellg());

	//読み込み位置をファイル先頭へ戻す
	fstr.seekg(0, fstr.beg);

	//動的配列を使ってファイルを読み込む場所を確保
	// charをfile_size個、メモリに確保する
	std::vector<char> texture_buffer(file_size);

	//確保した場所へファイルの内容をすべて読み込む
	fstr.read(&texture_buffer[0], file_size);

	//OpenGLに
	//「これから、テクスチャ識別子idに対して指示を与えます」
	//と指示
	glBindTexture(GL_TEXTURE_2D, id);

	//1ピクセルに「赤、緑、青、アルファ」の情報を持つ
	//幅256ピクセル、高さ256ピクセルの画像データをOpenGLへ転送
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 256, 256, 0, GL_RGBA, GL_UNSIGNED_BYTE, &texture_buffer[0]);

	//画像が拡大された場合にどう振舞うか指定
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	//画像が縮小された場合にどう振舞うか指定
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	return true;
}

int main(){
	if (!glfwInit()){
		return EXIT_FAILURE;
	}

	if (!glfwOpenWindow(0, 0, 0, 0, 0, 0, 0, 0, GLFW_WINDOW)){
		glfwTerminate();
		return EXIT_FAILURE;
	}

	//OpenGLにテクスチャ識別子を1つ作ってもらう
	GLuint texture_id;
	glGenTextures(1, &texture_id);

	//画像ファイルを読み込んで
	//テクスチャ識別子に設定する
	if (!setupTexture(texture_id, "sample.raw")){
		//画像の読み込みに失敗したらテクスチャ識別子を削除、GLFWの後始末をして終了
		glDeleteTextures(1, &texture_id);
		glfwTerminate();
		return EXIT_FAILURE;
	}
	glfwSwapInterval(1);

	while (glfwGetWindowParam(GLFW_OPENED)){
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		//描画する矩形の4頂点を配列で用意
		static const GLfloat vtx[] = {
			-0.5f, -0.5f,
			0.5f, -0.5f,
			0.5f, 0.5f,
			-0.5f, 0.5f
		};
		glVertexPointer(2, GL_FLOAT, 0, vtx);

		//頂点ごとのテクスチャ座標を配列で準備
		static const GLfloat texture_uv[] = {
			0.0f, 1.0f,
			1.0f, 1.0f,
			1.0f, 0.0f,
			0.0f, 0.0f
		};
		glTexCoordPointer(2, GL_FLOAT, 0, texture_uv);

		//OpenGLにテクスチャによる描画を有効にすると指示
		glEnable(GL_TEXTURE_2D);

		glEnableClientState(GL_VERTEX_ARRAY);

		//描画の時にテクスチャ座標配列も使うと指示
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);

		//矩形を1つ描画
		glDrawArrays(GL_QUADS, 0, 4);

		//描画が済んだら使った機能をすべて無効にする
		glDisableClientState(GL_VERTEX_ARRAY);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		glDisable(GL_TEXTURE_2D);

		glfwSwapBuffers();
	}

	//使い終わったテクスチャ識別子を削除
	glDeleteTextures(1, &texture_id);

	glfwTerminate();

	return EXIT_SUCCESS;
}